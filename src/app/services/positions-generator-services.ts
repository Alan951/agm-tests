import { Position } from './positions-generator-services';
import { Injectable } from '@angular/core';
//import { BBox, randomPoint } from '@turf/turf';
import * as turf from '@turf/turf';

@Injectable({
  providedIn: 'root'
})
export class PositionsGeneratorService {

  constructor() { }

  public generatePositions(quanty: number, box: turf.BBox): Array<Position>{
    let positions: Array<Position> = new Array<Position>();
    let indx: number = 0;


    while(indx < quanty){
      let latlng: number[] = turf.randomPosition(box);

      positions.push({
        latitud: latlng[0],
        longitud: latlng[1],
        name: indx
      } as Position)

      indx++;
    }

    console.log("positionsGenerated", positions)

    return positions;
  }

}

export interface Position{
  latitud: number;
  longitud: number;
  name: number
}
